package com.songoda.ycommandblocker.utils;

import com.songoda.ycommandblocker.YCommandBlocker;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.Listener;

import java.util.Arrays;

/**
 * Created by songoda on 6/4/2017.
 */
public class SettingsManager implements Listener {

    private final YCommandBlocker plugin;

    public SettingsManager(YCommandBlocker plugin) {
        this.plugin = plugin;
    }
    
    public void updateSettings(YCommandBlocker plugin) {
        for (settings s : settings.values()) {
            if (s.setting.equals("Upgrade-particle-type")) {
                if (plugin.v1_7 || plugin.v1_8) {
                    plugin.getConfig().addDefault("settings." + s.setting, "WITCH_MAGIC");
                }
                plugin.getConfig().addDefault("settings." + s.setting, s.option);
            } else
                plugin.getConfig().addDefault("settings." + s.setting, s.option);
        }

        ConfigurationSection cs = plugin.getConfig().getConfigurationSection("settings");
        for (String key : cs.getKeys(true)) {
            if (!key.contains("levels")) {
                if (!contains(key)) {
                    plugin.getConfig().set("settings." + key, null);
                }
            }
        }
    }

    public static boolean contains(String test) {
        for (settings c : settings.values()) {
            if (c.setting.equals(test)) {
                return true;
            }
        }
        return false;
    }

    public enum settings {

        o1("Main.Restricted Y Height", 15),
        o2("Main.Restricted Commands", Arrays.asList("tp", "tpa:20"));

        private String setting;
        private Object option;

        settings(String setting, Object option) {
            this.setting = setting;
            this.option = option;
        }

    }
}
