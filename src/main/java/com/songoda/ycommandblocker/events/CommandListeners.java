package com.songoda.ycommandblocker.events;

import com.songoda.ycommandblocker.Lang;
import com.songoda.ycommandblocker.YCommandBlocker;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Created by songoda on 3/14/2017.
 */
public class CommandListeners implements Listener {

    private final YCommandBlocker plugin;

    public CommandListeners(final YCommandBlocker plugin) {
        this.plugin = plugin;
    }


    @EventHandler(priority = EventPriority.HIGHEST)
    public void onCommand(PlayerCommandPreprocessEvent e) {
        Player p = e.getPlayer();
        if (p.hasPermission("YCommandBlocker.bypass")) {
            return;
        }
        for (String cmd : plugin.getConfig().getStringList("settings.Main.Restricted Commands")) {

            int height = plugin.getConfig().getInt("settings.Main.Restricted Y Height");

            if (cmd.contains(":")) {
                height = Integer.parseInt(cmd.split(":")[1]);
                cmd = cmd.split(":")[0];
            }

            if (p.getLocation().getY() > height) {
                continue;
            }

            String cm = e.getMessage().split(" ")[0].toUpperCase().substring(1);

            if (!cm.equals(cmd.toUpperCase())) {
                continue;
            }

            p.sendMessage(Lang.CANCEL_MESSAGE.getConfigValue());
            e.setCancelled(true);
        }

    }

}
