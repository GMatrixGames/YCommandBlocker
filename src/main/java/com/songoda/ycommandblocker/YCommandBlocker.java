package com.songoda.ycommandblocker;

import com.songoda.arconix.api.methods.formatting.TextComponent;
import com.songoda.ycommandblocker.events.CommandListeners;
import com.songoda.ycommandblocker.handlers.CommandHandler;
import com.songoda.ycommandblocker.utils.ConfigWrapper;
import com.songoda.ycommandblocker.utils.SettingsManager;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by songoda on 1/23/2018.
 */
public class YCommandBlocker extends JavaPlugin implements Listener {
    public static CommandSender console = Bukkit.getConsoleSender();

    private static YCommandBlocker INSTANCE;

    public SettingsManager sm;

    private ConfigWrapper langFile = new ConfigWrapper(this, "", "lang.yml");

    public boolean v1_7 = Bukkit.getServer().getClass().getPackage().getName().contains("1_7");
    public boolean v1_8 = Bukkit.getServer().getClass().getPackage().getName().contains("1_8");

    public void onEnable() {
        INSTANCE = this;
        console.sendMessage(TextComponent.formatText("&a============================="));
        console.sendMessage(TextComponent.formatText("&7YCommandsBlocker " + this.getDescription().getVersion() + " by &5Brianna <3&7!"));
        console.sendMessage(TextComponent.formatText("&7Action: &aEnabling&7..."));

        sm = new SettingsManager(this);
        setupConfig();
        langFile.createNewFile("Loading Language File", "YCommandBlocker Language File");
        loadLanguageFile();

        getServer().getPluginManager().registerEvents(new CommandListeners(this), this);

        this.getCommand("YCommandBlocker").setExecutor(new CommandHandler(this));

        this.getServer().getPluginManager().registerEvents(this, this);

        console.sendMessage(TextComponent.formatText("&a============================="));
    }

    public void onDisable() {
        console.sendMessage(TextComponent.formatText("&a============================="));
        console.sendMessage(TextComponent.formatText("&7YCommandsBlocker " + this.getDescription().getVersion() + " by &5Brianna <3!"));
        console.sendMessage(TextComponent.formatText("&7Action: &cDisabling&7..."));
        console.sendMessage(TextComponent.formatText("&a============================="));
    }

    public void reload() {
        langFile.createNewFile("Loading Language File", "YCommandBlocker Language File");
        loadLanguageFile();
        reloadConfig();
        saveConfig();
    }

    private void setupConfig() {
        sm.updateSettings(this);
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    private void loadLanguageFile() {
        Lang.setFile(langFile.getConfig());

        for (final Lang value : Lang.values()) {
            langFile.getConfig().addDefault(value.getPath(), value.getDefault());
        }

        langFile.getConfig().options().copyDefaults(true);
        langFile.saveConfig();
    }

    public static YCommandBlocker getInstance() {
        return INSTANCE;
    }
}
