package com.songoda.ycommandblocker.handlers;

import com.songoda.arconix.api.methods.formatting.TextComponent;
import com.songoda.ycommandblocker.Lang;
import com.songoda.ycommandblocker.YCommandBlocker;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Created by songoda on 3/14/2017.
 */

public class CommandHandler implements CommandExecutor {

    private final YCommandBlocker plugin;

    public CommandHandler(final YCommandBlocker plugin) {
        this.plugin = plugin;
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
        if (!sender.hasPermission("ycommandblocker.admin")) {
            sender.sendMessage(Lang.NO_PERMS.getConfigValue());
            return true;
        }
        if (args.length == 0) {
            sender.sendMessage("");
            sender.sendMessage(TextComponent.formatText("&6&l" + plugin.getDescription().getName() + " &7" + plugin.getDescription().getVersion() + " Created by &5&l&oBrianna"));
            sender.sendMessage(TextComponent.formatText("&6&l/ycb &7- Reload"));
            sender.sendMessage("");
        } else if (args[0].equalsIgnoreCase("reload")) {
            plugin.reload();
            sender.sendMessage("");
            sender.sendMessage(TextComponent.formatText("&eConfiguration and Language files reloaded."));
            sender.sendMessage("");
        } else {
            sender.sendMessage("");
            sender.sendMessage(Lang.COMMAND_NOT_FOUND.getConfigValue());
            sender.sendMessage("");
        }

        return true;
    }
}