package com.songoda.ycommandblocker;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

public enum Lang {

    CANCEL_MESSAGE("Cancel-Message", "&4(!) &cYou need to be higher up to use that command."),
    COMMAND_NOT_FOUND("Command-Not-Found", "&4(!) &cI'm sorry there must be some kind of mistake, this command does not exist."),
    NO_PERMS("No-perms", "&4(!) &cYou do not have permission to do that.");

    private String path;
    private String def;
    private static FileConfiguration LANG;

    Lang(String path, String start) {
        this.path = path;
        this.def = start;
    }

    public static void setFile(final FileConfiguration config) {
        LANG = config;
    }

    public String getDefault() {
        return this.def;
    }

    public String getPath() {
        return this.path;
    }

        public String getConfigValue() {
        return ChatColor.translateAlternateColorCodes('&', LANG.getString(this.path, this.def));
    }
}
